# Copyright (C) 2008 Canonical Limited.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

"""Tests for bzr-fastimport."""


from bzrlib.tests.TestUtil import TestLoader, TestSuite


def test_suite():
    module_names = [
        'bzrlib.plugins.fastimport.tests.test_branch_mapper',
        'bzrlib.plugins.fastimport.tests.test_commands',
        'bzrlib.plugins.fastimport.tests.test_errors',
        'bzrlib.plugins.fastimport.tests.test_filter_processor',
        'bzrlib.plugins.fastimport.tests.test_generic_processor',
        'bzrlib.plugins.fastimport.tests.test_head_tracking',
        'bzrlib.plugins.fastimport.tests.test_helpers',
        'bzrlib.plugins.fastimport.tests.test_parser',
        ]
    loader = TestLoader()
    return loader.loadTestsFromModuleNames(module_names)
